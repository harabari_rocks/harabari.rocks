const TextToSVG = require("text-to-svg");
const textToSVG = TextToSVG.loadSync("FredokaOne.ttf");
const fs = require("fs");

const options = { x: 0, y: 0, fontSize: 100, anchor: "top" };

const svg = textToSVG.getSVG("HARABARI.rocks", options);

fs.unlink("text.svg", err => {});
fs.writeFile("text.svg", svg, function(err) {
  if (err) {
    return console.log(err);
  }

  console.log("The file was saved!");
});
