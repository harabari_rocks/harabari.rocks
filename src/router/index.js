import Vue from 'vue'
import Router from 'vue-router'
import FontTest from '@/components/FontTest'
import FrontPage from '@/components/FrontPage'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: () => import('../components/FrontPage')
    },
    {
      path: '/FontTest',
      name: 'FontTest',
      component: () => import('../components/FontTest')
    }
  ]
})
