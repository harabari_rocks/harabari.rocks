const paper = require("paper");
const $ = require("jquery");
export let destroy = function(){
  paper.remove()
}

export let animateBanner = function() {

    let canvas = $("#banner");
    paper.setup(canvas[0]);

    let cwidth = 0;
    let cheight = 0;
    let txth;
    let txtv;
    let svg = $(require("../assets/HARABARI.rocks.svg"));
    svg.css("fill", canvas.css("fill"));
    let txtorig = paper.project.importSVG(svg[0]);
    txtorig.remove();

    let mousePos = new paper.Point(0, -1000);
    let mousePosOld = mousePos.clone();

    paper.tool = new paper.Tool();

    let ml;
    let mm;
    let chasing = false;
    let caught = false;
    let target = new paper.Point();

    paper.tool.onMouseMove = function(event) {
      target = event.point;
      clearInterval(ml);
      if (!(chasing || caught)) {
        chasing = true;
        mm = setInterval(() => {
          if (mousePos.getDistance(target) > 10) {
            mousePos = mousePos.add(target.subtract(mousePos).divide(10));
          } else {
            caught = true;
            clearInterval(mm);
          }
        }, 10);
      } else if (caught) {
        mousePos = event.point;
      }
    };

    canvas.mouseleave(e => {
      setTimeout(() => {
        clearInterval(mm);
        chasing = false;
        caught = false;
        mousePosOld = mousePos.clone();
        ml = setInterval(() => {
          if (mousePos.y > -1000) {
            mousePos.y += (-1000 - mousePosOld.y) / 100;
          } else {
            clearInterval(ml);
          }
        }, 10);
      }, 100);
    });

    function setup(e) {
      cwidth = canvas.width();
      cheight = canvas.height();

      paper.view.viewSize = new paper.Size(cwidth, cheight);
      let np = new paper.Point(cwidth / 2, cheight / 2);
      txtorig.position = np;
      paper.project.activeLayer.removeChildren();
      let txtcpy = txtorig.clone({ deep: true });
      if (cwidth < 1000){
        let ratio = cwidth / 1000;
        txtcpy.scale(ratio)
      }
      txth = txtcpy.clone({ deep: true });
      txtv = txtcpy.clone({ deep: true });
      txth.visible = false;
      txtv.visible = true;

      paper.project.activeLayer.addChild(txtv);

      for (let ip = 0; ip < txtv.firstChild.children.length; ip++) {
        let path = txtv.firstChild.children[ip];
        for (let is = 0; is < path.segments.length; is++) {
          path.segments[is].point.delta = 0;
        }
      }
      paper.view.draw();
    }

    setup();

    paper.view.onResize = setup;

    paper.view.on("frame", e => {
      for (let ip = 0; ip < txth.firstChild.children.length; ip++) {
        let path = txth.firstChild.children[ip];
        for (let is = 0; is < path.segments.length; is++) {
          const point = path.segments[is].point;
          let pos = point.clone();
          let d = pos.getDistance(mousePos);
          txtv.firstChild.children[ip].segments[is].point.delta += e.delta;
          let dl = txtv.firstChild.children[ip].segments[is].point.delta;
          pos.y +=
            Math.sin(dl * 5 + pos.x / 15) * ((1 - Math.tanh(d / 300)) * 4 + 0.5);
          txtv.firstChild.children[ip].segments[is].point = pos;
        }
      }
    });

    paper.view.play();
};
