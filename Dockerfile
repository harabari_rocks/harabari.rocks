FROM node:alpine
MAINTAINER Victor Harabari

RUN mkdir /website
RUN cd /website
RUN npm i -g http-server

COPY dist/ /website/

WORKDIR /website

EXPOSE 8080

CMD ["http-server","."]